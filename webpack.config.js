module.exports = {
  context: __dirname, // we just want to use $cwd
  // entry is the file that renders our React app.
  entry: {
    wizrd: [
      './wizrd.js'
    ]
  },
  externals: {
    'lodash': 'umd lodash',
    'prop-types': 'umd prop-types',
    'react': 'umd react',
    'react-dom': 'umd react-dom',
    'react-redux': 'umd react-redux',
    'redux': 'umd redux'
  },
  mode: 'none',
  module: {
    // our loaders are our transpilers and interpreters such as Babel
    rules: [{
      test: /\.jsx?$/,
      use: 'babel-loader',
    }]
  },
  output: {
    path: __dirname + '/dist',
    // filename is the name of the output file that will be compiled by Webpack
    filename: '[name].js',
    publicPath: '/',
    library: 'WizrD',
    libraryTarget: 'umd'
  },
  resolve: {
    extensions: ['.js', '.jsx']
  }
};
