import { connect, Provider } from 'react-redux';
import { createStore }       from 'redux';
import find                  from 'lodash/find';
import findIndex             from 'lodash/findIndex';
import isEmpty               from 'lodash/isEmpty';
import partial               from 'lodash/partial';
import PropTypes             from 'prop-types';
import React, { Component }  from 'react';

import reducer               from './reducer';


const wizrdStore = createStore(reducer);

export function Root({ store = wizrdStore, ...props } = {}) {
  return (
    props.isStateful
    ? (
      <Provider store={store}>
        <ConnectedWizrD {...props} />
      </Provider>
    ) : (
      <WizrD {...props} />
    )
  );
}

export class WizrD extends Component {
  constructor(props) {
    super(props);

    this.changeChoice = this.changeChoice.bind(this);
    this.getCurrentBranch = this.getCurrentBranch.bind(this);
    this.getCurrentStep = this.getCurrentStep.bind(this);
    this.isFinalStep = this.isFinalStep.bind(this);
    this.renderControls = this.renderControls.bind(this);
    this.renderStep = this.renderStep.bind(this);
    this.STEPS = this.props.steps;
    this.state = { currentBranch: 0, currentChoice: 0, currentStep: 0 };
  }

  changeChoice({ componentName, isNextStep }) {
    const currentStep = (
      isNextStep
        // This should always be 0
        ? findIndex(this.STEPS, step => (
          !isEmpty(find(
            step.branches,
            branch => branch.key === componentName
          ))
        )) : this.state.currentStep
    );

    const currentBranch = (
      currentStep === this.state.currentStep
      ? findIndex(
        this.getCurrentStep(this.state.currentStep).branches,
        branch => branch.key === componentName
      ) : 0
    );

    this.setState({ currentStep, currentBranch });
  }

  getCurrentBranch(branchIndex, stepIndex) {
    return this.getCurrentStep(stepIndex).branches[branchIndex];
  }

  getCurrentChoice(branchIndex, choiceIndex, stepIndex) {
    return this.getCurrentBranch(branchIndex, stepIndex).choices[choiceIndex];
  }

  getCurrentStep(stepIndex) {
    return this.STEPS[stepIndex];
  }

  isFinalStep({ currentBranch, currentChoice, currentStep }) {
    return (
      currentStep === this.STEPS.length - 1 &&
      currentBranch === this.STEPS[currentStep].branches.length - 1 &&
      currentChoice === this.getCurrentBranch(currentBranch, currentStep).choices.length - 1
    );
  }

  /* NOTE: Need to refactor this to be more function and declare less variables
   * (particularly with finding the displayName)
   */
  renderControls() {
    const { currentBranch, currentChoice, currentStep } = this.state;
    const currentStepBranches = this.getCurrentStep(this.state.currentStep).branches;
    const currentBranchData = this.getCurrentBranch(currentBranch, currentStep);

    const controlWidth = (
      this.isFinalStep({ currentBranch, currentChoice, currentStep })
      ? 6 : (12 / currentBranchData.choices.length)
    );

    return (
      currentBranchData.choices.map(choice => {
        const { componentName, prerequisiteForms, isNextStep } = choice;
        const displayName = isNextStep ? choice.displayName : find(
          currentStepBranches,
          branch => branch.key === componentName
        ).displayName;

        /* NOTE: Future improvement: allow passing in of validation function to
         * toggle disable attribute
         */
        return (
          <div className={`col col-xs-${controlWidth} wizrd-control`}>
            <button
              disabled={
                isEmpty(prerequisiteForms)
                ? false
                : prerequisiteForms.every(name => isEmpty(this.props.data[name]))
              }
              onClick={partial(this.changeChoice, { componentName, isNextStep })}
            >
              {`Go to ${displayName}`}
            </button>
          </div>
        );
      }).concat((() => (
        this.isFinalStep({ currentBranch, currentChoice, currentStep })
        ? (
          <div className={`col col-xs-${controlWidth} wizrd-control`}>
            <button
              onClick={partial(
                this.props.onComplete,
                partial.placeholder,
                this.props.data
              )}
            >
              Finish
            </button>
          </div>
        ) : []
      ))())
    );
  }

  renderStep(step) {
    try {
      return React.createElement(
        step.branches[this.state.currentBranch].component,
        this.props
      );
    } catch (error) {
      throw new Error('Could not find step!');
    }

  }

  render() {
    return (
      <div className="row wizrd-step-and-controls">
        <div className="col col-xs-12">
          <div className="row wizrd-step-row">
            <div className="col col-xs-12 wizrd-step-column">
              {this.renderStep(this.STEPS[this.state.currentStep])}
            </div>
          </div>
          <div className="row">
            <div className="col col-xs-12">
              <div className="row wizrd-controls-row">
                {this.renderControls()}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

WizrD.defaultProps = {
  data: {},
  isStateful: false,
  onComplete: () => false,
  steps: []
};

const ConnectedWizrD = connect(
  storeState => storeState,
  dispatch => ({ dispatch })
)(WizrD);
