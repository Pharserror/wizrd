import clone from 'lodash/clone';
import isEmpty from 'lodash/isEmpty';
import merge from 'lodash/merge';


const INITIAL_STATE = {
  current: {
    flow: null,
    step: {},
    name: null
  },
  data: {},
  history: {
    steps: []
  },
  previous: {
    flow: null,
    step: {},
    name: null
  }
};

export default function reducer(state = INITIAL_STATE, action) {
  const newState = clone(state);

  switch (action.type) {
    case 'GO_TO_NEXT_STEP':
      return merge(
        {},
        newState,
        {
          currentStep:  action.step,
          history:      state.history.concat(action.step),
          previousStep: newState.current
        }
      );
    case 'GO_TO_PREVIOUS_STEP':
      const currentStep = state.history[state.history.length - 2];

      if (isEmpty(currentStep)) {
        throw new Error('Cannot go back any more steps!');
        return;
      }

      return merge(
        {},
        newState,
        {
          currentStep:  currentStep,
          history:      state.history.concat(currentStep),
          previousStep: state.history[state.history.legnth - 1]
        }
      );
    case 'ON_LOCK_FORM_DATA': {
      return {
        ...newState,
        data: action.payload
      };
    }
    default:
      return newState;
  }
};
